Puppet::Type.type(:serveradmin).provide(:settings) do
	require 'tempfile'
    @doc = "apply serveradmin settings for os x server"
	defaultfor :operatingsystem => :darwin
	commands :serveradmin => "/usr/sbin/serveradmin"

	def check
		pairs = ""
		begin
			execute("#{:serveradmin} settings '#{resource[:name]}'").split("\n").each do |l|
				pairs << "#{l}\n"
			end
		rescue Puppet::ExecutionFailure
			raise Puppet::Error.new("Unable to read serveradmin service: #{resource[:name]}")
		end
		debug("retrieve: Analyzing returned results: #{pairs}")
		debug("retrieve: found #{pairs.count('=')} lines of info")
		if pairs.count('=') < 2
			debug("retrieve: checking single line for empty array")
			if pairs.match(/\A.*(_empty_array).*$/)
				debug("retrieve: looks like and empty array")
				@data = $1
				return :empty
				elseif pairs.match(/\A.*(_empty_dictionary).*$/)
				debug("retrieve: found empty dictionary, looks like bogus setting")
				@data = $1
				return :outofsync
			end
		end
		case resource[:settings]
		when Hash
			debug("retrieve: command returned multiple lines, will split into hash")
			@data = Hash[*pairs.scan(/^#{Regexp.escape(resource[:name])}:(.*) = (.*)$/).flatten]
			debug("retrieve: Analyzing:\n #{@data.inspect}\n and\n #{resource[:settings].inspect}")
			resource[:settings].select{|k,v| return :outofsync if @data[k]!=v}
			return :insync
		when String
			debug("retrieve: scanning returned line: ...")
			debug("retrieve: results: #{pairs.scan(/^#{Regexp.escape(resource[:name])} = (.*)$/)}")
			@data = pairs.scan(/^#{Regexp.escape(resource[:name])} = (.*)$/)
			return :insync if @data.to_s == resource[:settings]
		end
		return :outofsync
	end

	def delete
		lines = Array.new
		begin
			execute("#{:serveradmin} settings '#{resource[:name]}' = delete")
		rescue Puppet::ExecutionFailure
			raise Puppet::Error.new("Unable to delete serveradmin: #{resource[:name]}")
		end
	end

	def write
		lines = Array.new
		begin
			case resource[:settings]
			when Hash
				resource[:settings].rehash
				resource[:settings].sort_by { |k, v| k.scan(/[0-9]+|[^0-9]+/).map {|s| s[/[^0-9]/] ? s : s.to_i} }.each do |k,v|
					lines << "#{resource[:name]}:#{k} = #{v}"
				end
			when String
				lines << "#{resource[:name]} = #{resource[:settings]}"
			else
				puts "Wrong class"
			end
			if @data == "_empty_array"
				debug("empty array: needs #{resource[:name]} = create")
				lines.unshift("#{resource[:name]} = create")
			end
			set_value(lines)
			ensure
			lines = nil
			@data = nil
		end
	end

	private

	def set_value( values )
		cmd = "echo '#{values.join('\n')}' | #{:serveradmin} settings"
		commandOutput = ""
		begin
			execute(cmd).split("\n").each do |l|
				commandOutput << "#{l}\n"
			end
		rescue Puppet::ExecutionFailure
			raise Puppet::Error.new("Unable to modify serveradmin setting: #{resource[:name]}")
		end

		return commandOutput
	end

end
